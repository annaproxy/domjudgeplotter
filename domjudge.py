"""
DomJudge Hacky Plotter
By Anna Langedijk

How to use:

Log in to DOMJudge jury page
To plot submissions:
 - Go to submissions, click show all
 - Copy all contents (e.g. ctrl+a, ctrl+c) and paste them to a textfile
 - Pass the file to --submissions_file

To plot clarification requests set to answered:
 - Go to clarifications
 - Copy all contents and paste them to a textfile
 - Pass the file to --clarifications_file
"""
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import seaborn as sns
import argparse


def replace_dates(s: str) -> str:
    """Replaces dates in a string hackily with correct format"""
    return (
        s.replace("Feb", "02 2021")
        .replace("Mar", "03 2021")
        .replace("Jan", "01 2021")
        .replace("Apr", "04 2021")
    )


def parse_clarifications(
    filename: str, start_date: datetime.date, end_date: datetime.date
) -> pd.DataFrame:
    """
    hackily parse a file copy-pasted from clarifications page
    """
    with open(filename) as f:
        c = f.read().split(
            "General Clarifications:\nID	contest	time	from	to	subject	text	answered	by"
        )

        specific_c = (
            c[0]
            .split("ID	contest	time	from	to	subject	text	answered	by")[1]
            .split("default")
        )
        general_c = c[1]
        names_set = set()
        asked_times = []
        answerers = []
        for i, c in enumerate(specific_c):
            # skip non-clarification
            if i == 0:
                continue
            c_info = c.split("\n\t\n")

            test = datetime.strptime(replace_dates(c_info[1]), "%d %m %Y %H:%M")
            answerer = c_info[-2].split("\n")[0]
            # padding at start
            if answerer not in names_set:
                asked_times.append(start_date)
                answerers.append(answerer)
                names_set.add(answerer)

            asked_times.append(test)
            answerers.append(answerer)
        # padding at end
        for t in names_set:
            asked_times.append(end_date)
            answerers.append(t)
        df_clar = pd.DataFrame({"date": asked_times, "answerer": answerers})
    return df_clar


def parse_submissions(
    filename: str, start_date: datetime.date, end_date: datetime.date
) -> pd.DataFrame:
    """
    hackily parse a file copy-pasted from submissions page
    """
    with open(filename) as f:
        # Tabs MAY POSSIBLY not work if you copy-paste with different browsers/os. lol
        test = f.read().split("ID	time	team	problem	lang	result	verified	by")[1]
        all_submissions = test.split("claim")

        datetimes = []
        types = []

        types_set = set()
        names = []
        for submission in all_submissions:
            submission_info = submission.split("\n\t\n")
            if len(submission_info) < 2:
                break
            #print(submission_info)
            test = datetime.strptime(
                replace_dates(submission_info[1]), "%d %m %Y %H:%M"
            )

            # Maybe possibly do something with this to automatically filter
            problem_name = submission_info[3]

            sort_of_submission = submission_info[5]

            # Pad the values at start
            if sort_of_submission not in types_set:
                datetimes.append(start_date)
                types.append(sort_of_submission)
                types_set.add(sort_of_submission)
                names.append("None")

            types.append(sort_of_submission)
            datetimes.append(test)
            names.append(submission_info[2])

        # pad values at the end 
        for t in types_set:
            datetimes.append(end_date)
            types.append(t)
            names.append("None")

        df_sub = pd.DataFrame(
            {"date": datetimes, "type": types, "name": names}, index=datetimes
        )
    return df_sub


def plot(df: pd.DataFrame, clars: bool = False, relative: bool = False):
    """
    Plot submissions or clarifications according to the `clars` bool.
    Plot relative (divided by cumulative amount of EVERYTHING at that timestep)
    """

    if clars:
        # Sorted by amount, the "best" TA will be on top
        cdict = df["answerer"].value_counts()
        cdict = list(reversed(list(cdict.index)))  # python hacking
        colors = np.array(list(sns.color_palette("husl", len(cdict))))
        cdict = {k: v for k, v in zip(cdict, colors)}
    else:
        # Custom colors somehow reflecting type of submission
        cdict = {
            "correct": "limegreen",
            "no-output": "cadetblue",
            "wrong-answer": "red",
            "compiler-error": "gray",
            "run-error": "mediumvioletred",
            "timelimit": "darkorange",
        }

    # Bookkeeping
    all_datapoints = df.groupby(["date"]).size().cumsum() - len(cdict)

    last = df["date"].max()
    first = df["date"].min()

    interval = pd.interval_range(
        start=first, end=last, freq=pd.tseries.offsets.DateOffset(minutes=1)
    )
    zeros = np.zeros((len(interval)))
    prev = None

    # initialize plot
    #print(len(interval))
    plt.figure(figsize=(len(interval)/1200, 6)) # Hackily set plot width
    plt.tight_layout()

    # Loop over all values and stack them in the plot
    for clar_type in cdict.keys():
        filtered_cumulative_df = (
            df[df["type" if not clars else "answerer"] == clar_type]
            .groupby(["date"])
            .size()
            .cumsum()
        )

        resampled_times = filtered_cumulative_df.resample("1min").ffill() - 1
        if relative:
            resampled_times /= all_datapoints.resample("1min").ffill()

        plt.plot(
            resampled_times + (prev if prev is not None else 0), color=cdict[clar_type]
        )
        if prev is None:
            plt.fill_between(
                resampled_times.index,
                0,
                resampled_times,
                color=cdict[clar_type],
                label=clar_type if not clars else clar_type.split(" ")[0],
                alpha=0.6,
            )
            prev = resampled_times

        else:
            plt.fill_between(
                resampled_times.index,
                prev,
                prev + resampled_times,
                label=clar_type if not clars else clar_type.split(" ")[0],
                color=cdict[clar_type],
                alpha=0.6,
            )
            prev += resampled_times

    # Customize plot
    plt.xticks()
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%m-%d"))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.setp(plt.gca().xaxis.get_majorticklabels(), rotation=45)
    for tick in plt.gca().xaxis.get_major_ticks():
        tick.label1.set_horizontalalignment("center")
        tick.set_pad(2.5 * tick.get_pad())
    plt.gca().xaxis.set_minor_formatter(mdates.DateFormatter("%H"))
    plt.gca().xaxis.set_minor_locator(mdates.HourLocator(interval=6))

    plt.legend(loc="upper left")
    plt.ylabel("Cumulative amount" if not relative else "Relative cumulative amount")
    plt.xlabel("Day and time")
    # plt.grid()
    if not clars:
        plt.title("Submission overview for Practical 1")
    else:
        plt.title("Clarification sent")

    # Save plot
    filename = "clarifications_plot" if clars else "submissions_plot"
    filename += "_relative" if relative else ""
    plt.savefig(f"{filename}.pdf", bbox_inches="tight")


def main(args):

    start_date = datetime.strptime(args.start_date, "%Y-%m-%d")
    end_date = datetime.strptime(args.end_date, "%Y-%m-%d")

    df_sub = parse_submissions(
        args.submissions_file, start_date=start_date, end_date=end_date
    )
    df_clar = parse_clarifications(
        args.clarifications_file, start_date=start_date, end_date=end_date
    )

    # Filter datetime
    df_sub = df_sub[(df_sub["date"] >= start_date) & (df_sub["date"] <= end_date)]
    df_clar = df_clar[(df_clar["date"] >= start_date) & (df_clar["date"] <= end_date)]
    df_clar = df_clar[df_clar["answerer"] != "yes"]  # Filter out the hacks
    # df_sub = df_sub[df_sub["name"] != "Anna Langedijk"] # Used to strip TA submissions

    with plt.style.context("ggplot"): # customize style 
        # Plot absolute
        plot(df_clar, True)
        plot(df_sub, False)
        # Plot relative
        plot(df_clar, True, relative=True)
        plot(df_sub, False, relative=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot DOMJudge info")
    parser.add_argument(
        "--clarifications_file",
        type=str,
        help="Where the clarifications textfile is stored.\
        This textfile can be created by pressing Ctrl+A on the clarifications page on DOMJudge jury",
        default="clarifications.txt",
    )
    parser.add_argument(
        "--submissions_file",
        type=str,
        help="Where the submissions textfile is stored.\
        This textfile can be created by pressing Ctrl+A on the submissions page on DOMJudge jury\
        (after clicking 'all' - might take a while to load!)",
        default="submissions.txt",
    )
    parser.add_argument(
        "--start_date",
        type=str,
        help="Starting date for plot. Format: YYYY-MM-DD",
        default="2021-02-19",
    )

    parser.add_argument(
        "--end_date",
        type=str,
        help="End date for plot. Format: YYYY-MM-DD",
        default="2021-02-28",
    )
    args = parser.parse_args()
    main(args)


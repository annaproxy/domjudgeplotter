# What is it
Plots cumulative (relative) amount of submissions per submission type and clarification set to answered per answerer. For an example see `submissions_plot.pdf`.

# How to use

Log in to DOMJudge jury page

To plot submissions:
 - Go to submissions, click show all
 - Copy all contents (e.g. ctrl+a, ctrl+c) and paste them to a textfile
 - Pass the file to --submissions_file

To plot clarification requests set to answered:
 - Go to clarifications
 - Copy all contents and paste them to a textfile
 - Pass the file to --clarifications_file

Run `python domjudge.py --help`  for more info

# future feature ideas

  * [ ] Select which practical names to filter
  * [ ] Include general clarifications
  * [ ] More filter stuff